package module3;

import java.util.logging.Logger;

public class task2 {
  /*Description:
    Implement Binary Search Tree
  */

  public static Tree BinarySearch(Tree treeToSearch, int valueToSearch) {

    if (treeToSearch.value == valueToSearch)
      return treeToSearch;

    if (valueToSearch > treeToSearch.value) {
      return BinarySearch(treeToSearch.right, valueToSearch);
    }
    else
      return BinarySearch(treeToSearch.left, valueToSearch);
  }

  public static Tree insert(Tree node, int value) {

    if (node == null)
      return new Tree(value);

    if (value > node.value)
      node.right = insert(node.right, value);
    else if (value < node.value)
      node.left = insert(node.left, value);

    return node;
  }

  public static void task() {

    Logger logging = Logger.getLogger("java-training");

    Tree root = new Tree(9);
    insert(root, 4);
    insert(root, 2);
    insert(root, 3);
    insert(root, 30);
    insert(root, 10);
    insert(root, 15);

    int valueToSearch = 30;

    try {
      Tree result = BinarySearch(root, valueToSearch);
      logging.info(String.valueOf(result.value));
    }
    catch (NullPointerException exception) {
      logging.warning("Value: " + valueToSearch + ", is not in the tree.");
    }
  }

  public static void main(String[] args) { task(); }
}
