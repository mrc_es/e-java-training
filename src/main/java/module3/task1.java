package module3;

import java.util.Scanner;
import java.util.logging.Logger;

public class task1 {
  /*Description:
  Write a function that takes a number as an argument.

  The function should return the n* number of the Fibonacci sequence
  Ask for the number to the user
  Print first 5 numbers of Fibonacci series
  Calculate the 40th number of the Fibonacci sequence
  */

  public static void printFibonacciUntil(int value)  {
    for (int i = 0; i <= value; i++)
      System.out.println(fibonacci(i));
  }

  public static int fibonacci(int n) {

    if (n == 1)
      return 1;
    else if (n == 0)
      return 0;

    return fibonacci(n - 1) + fibonacci(n - 2);
  }

  public static void task() {

    Logger logging = Logger.getLogger("java-training");

    Scanner scanner = new Scanner(System.in);

    System.out.println("Enter the number of fibonacci sequence you want: ");
    int nthValue = Integer.parseInt(scanner.nextLine());

    int fibonacciValue = fibonacci(nthValue);
    logging.info(String.valueOf(fibonacciValue));

    printFibonacciUntil(6);

  }

  public static void main(String[] args) { task(); }
}
