package module1;

import java.util.HashSet;
import java.util.TreeSet;
import java.util.logging.Logger;

public class Task4 {

  /*Description:
  * Write a Java program to convert a hash set to a tree set
  */

  public static void task() {

    Logger logging = Logger.getLogger("java-training");

    final HashSet<String> authors = new HashSet<>();
    authors.add("Samuel Beckett");
    authors.add("William Faulkner");
    authors.add("D. H. Lawrence");
    authors.add("Franz Kafka");
    authors.add("Henry Miller");
    authors.add("Jack Kerouac");

    final TreeSet<String> authorsTreeSet = new TreeSet<>(authors);

    int numAuthor = 1;
    for (String author: authorsTreeSet) {
      logging.info("Author " + (numAuthor++) + ": " + author);
    }


  }
  public static void main(String[] args) { task(); }
}
