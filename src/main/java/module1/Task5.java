package module1;

import java.util.LinkedList;
import java.util.logging.Logger;

public class Task5 {
  /*
  * Description:
  * Write a Java program to replace an element in a linked list
  */

  public static void replaceInLinkedList(LinkedList<String> linkedList,
                                         String elementToReplace,
                                         String replacement) {

    int indexToReplace = linkedList.indexOf(elementToReplace);
    linkedList.set(indexToReplace, replacement);

  }

  public static void task() {

    Logger logging = Logger.getLogger("java-training");

    final String elementToReplace = "The Trial";
    final String replacement = "The Castle";

    LinkedList<String> books = new LinkedList<>();
    books.add("Molloy");
    books.add("The Unnameable");
    books.add("The Trial");
    books.add("Lady Chatterley's Lover");

    replaceInLinkedList(books, elementToReplace, replacement);

    int bookNumber = 0;
    for (String book: books)
      logging.info("Book " + (bookNumber++) + ": " + book);

  }
  public static void main(String[] args) { task(); }

}
