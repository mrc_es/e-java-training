package module1;

import java.util.logging.Logger;

public class Task1 {

  /*
  Description
  Write a program to show the basic arithmetic Operators.

  Define numeric variables

  Print result of every operation:

  - Addition
  - Subtraction
  - Multiplication
  - Division
  */

  public static void task() {

    Logger logging = Logger.getLogger("java-training");

    final int first = 5;
    final int second = 5;

    int sum = first + second;
    int sub = first - second;
    int multiplication = first * second;
    int division = first / second;

    logging.info("sum: " + sum);
    logging.info("sub: " + sub);
    logging.info("multiplication: " + multiplication);
    logging.info("division: " + division);
  }

  public static void main(String[] args) { task(); }

}
