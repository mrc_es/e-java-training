package module1;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.logging.Logger;

public class Task6 {
  /*
  * Description:
  * Write a Java program to sort elements of an ArrayList
  */
  public static void task() {

    Logger logging = Logger.getLogger("java-training");

    final ArrayList<Integer> arrayListToBeOrdered = new ArrayList<>();
    arrayListToBeOrdered.add(2);
    arrayListToBeOrdered.add(1);
    arrayListToBeOrdered.add(4);
    arrayListToBeOrdered.add(3);

    arrayListToBeOrdered.sort(Comparator.naturalOrder());

    for (int element: arrayListToBeOrdered)
      logging.info(String.valueOf(element));


  }
  public static void main(String[] args) { task(); }

}
