package module1.task3Classes;

public class Snake extends Animal {

  public void bites() {
    System.out.println("It bites");
  }

  public void breathe() {
    System.out.println("Breathes but better");
  }

  public void breathe(int timesBetter) { System.out.println("Breathes " + timesBetter + "x better");  }

}
