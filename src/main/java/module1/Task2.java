package module1;

import java.util.logging.Logger;

public class Task2 {

  /*
  Description
  Write a program to show the logical operators behavior.

  1. Define Boolean variables

  2. Print the result of comparison

  - Logical AND
  - Logical OR
  - Logical Not
  */

  static void task() {

    Logger logging = Logger.getLogger("java-training");

    final boolean boolTrue1 = true;
    final boolean boolTrue2 = true;
    final boolean boolFalse1 = false;
    final boolean boolFalse2 = false;

    logging.info("True && True: " + (boolTrue1 && boolTrue2));
    logging.info("True && False: " + (boolTrue1 && boolFalse1));
    logging.info("False && True: " + (boolFalse1 && boolTrue1));
    logging.info("False && False: " + (boolFalse1 && boolFalse2));

    logging.info("True || True: " + (boolTrue1 || boolTrue2));
    logging.info("True || False: " + (boolTrue1 || boolFalse1));
    logging.info("False || True: " + (boolFalse1 || boolTrue1));
    logging.info("False || False: " + (boolFalse1 || boolFalse2));

    logging.info("!True: " + (!boolTrue1 ));
    logging.info("!False: " + (!boolFalse1));

    logging.info("!(True && True): " + !(boolTrue1 && boolTrue2));
    logging.info("!(False || False): " + !(boolFalse1 || boolFalse2));

  }

  public static void main(String[] args) { task(); }

}
