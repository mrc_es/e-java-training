package module1;

import module1.task3Classes.Snake;

public class Task3 {
  /*
  * Description:
  * Write a program to show the Method Overriding.
  *
  * Define a parent class
  * Define a child class
  * Create a main class to call them
  * Display the output
  */

  static void task() {

    Snake snake = new Snake();

    snake.bites();
    snake.moves();
    snake.breathe();
    snake.breathe(100);

  }

  public static void main(String[] args) {
    task();
  }

}
