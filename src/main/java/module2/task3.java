package module2;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class task3 {
  /*Description:
    Sort a List using Stream sorted method.
  */
  public static void task() {

    Logger logging = Logger.getLogger("java-training");

    ArrayList<Integer> unorderedList = new ArrayList<>();
    unorderedList.add(5);
    unorderedList.add(3);
    unorderedList.add(4);
    unorderedList.add(1);
    unorderedList.add(2);

    List<Integer> orderedList = unorderedList.stream()
        .sorted()
        .collect(Collectors.toList());

    orderedList.forEach((item) -> logging.info(String.valueOf(item)));

  }

  public static void main(String[] args) { task(); }
}
