package module2;

import java.util.logging.Logger;

public class task2 {
  /*Description:
    Using lambdas, print even numbers.
  */
  interface UnaryInterface {void operation(int a);}

  public static void task() {

    Logger logging = Logger.getLogger("java-training");

    UnaryInterface getEven = (x) -> {
      if (x % 2 == 0)
        logging.info(x + " is even");
    };

    for (int i = 0; i <= 10; i++)
      getEven.operation(i);

  }

  public static void main(String[] args) { task(); }
}
