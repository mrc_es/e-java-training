package module2;

import java.util.logging.Logger;

public class task1 {
  /*Description:

    Using lambdas, write a program to show the basic arithmetic operators
    Print result of every operation:
      - Addition
      - Subtraction
      - Multiplication
      - Division
  */
  interface BinaryInterface {int operation(int a, int b);}

  public static void task() {

    Logger logging = Logger.getLogger("java-training");

    int value1 = 4;
    int value2 = 2;

    BinaryInterface sum = Integer::sum;
    logging.info("Sum: " + sum.operation(value1, value2));

    BinaryInterface subtraction = (x, y) -> x - y;
    logging.info("Subs: " + subtraction.operation(value1, value2));

    BinaryInterface multiplication = (x, y) -> x * y;
    logging.info("Mult: " + multiplication.operation(value1, value2) );

    BinaryInterface division = (x, y) -> x / y;
    logging.info("Div: " + division.operation(value1, value2));

  }

  public static void main(String[] args) { task(); }
}
